#ifndef QUEUE_H
#define QUEUE_H

typedef struct point
{
  int x;
  int y;
} point;

typedef struct double_chained double_chained;

typedef struct queue queue;

queue* new_queue();
int empty_queue (queue*);
void add_point (queue*,point);
double_chained* get_head_chained (queue*);
point get_point_chained (double_chained* c);
double_chained* get_left_chained (double_chained* c);
double_chained* get_right_chained (double_chained* c);
void set_head (queue* q, double_chained* c);
point get_head (queue*);
point get_tail (queue*);
void pop_queue (queue*);

#endif
