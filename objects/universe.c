#include<ncurses.h>
#include "queue.h"
#include "snake.h"
#include "universe.h"
#include "seed.h"

char get_cell (char universe[], point p)
{
  return (universe[p.x + UNI_SIZE*p.y]);
}

void set_cell(char universe[], point p, char c)
{
  if (p.x >= 0 && p.x < UNI_SIZE &&
      p.y >= 0 && p.y < UNI_SIZE)
    universe[p.x + UNI_SIZE*p.y] = c;
}

void set_cell_update(char universe[], point p, char c)
{
  if (p.x >= 0 && p.x < UNI_SIZE &&
      p.y >= 0 && p.y < UNI_SIZE)
    {
      set_cell(universe,p,c);
      mvaddch(p.y,p.x,c);
      move(UNI_SIZE+1,0);
    }
}

int update_universe (char universe[], snake* s, queue* seeds_now, queue* seeds_previous)
{
  point tail;
  point head;
  point new_head;

  tail = get_tail(get_body(s));
  head = get_head(get_body(s));
  s = update_snake(s);
  new_head = get_head(get_body(s));

  if (get_cell(universe,new_head) == BACKGROUND)
    {
      set_cell_update(universe,head,SNAKE_BODY);
      pop_queue(get_body(s));
      set_cell_update(universe,tail,BACKGROUND);
      set_cell_update(universe,new_head,SNAKE_HEAD);
      return 1;
    }
  if (get_cell(universe,new_head) == SEED)
    {
      set_cell_update(universe,head,SNAKE_BODY);
      set_cell_update(universe,new_head,SNAKE_HEAD);
      put_seed(universe, seeds_now, seeds_previous);
      incr_len(s);
      move (UNI_SIZE,7);
      printw("%d", get_len(s) - 3);
      return 1;
    }
  return 0;
}

void print (char universe [])
{
  point p;

  move(0,0);
  for (p.y = 0 ; p.y < UNI_SIZE; p.y++)
  {
    for (p.x =0; p.x < UNI_SIZE; p.x++)
  	{
  	  printw("%c",get_cell(universe,p));
    }
    printw("\n");
  }
refresh();
}

void copy (char board[], char universe[])
{
  point p;

  for (p.y = 0; p.y <UNI_SIZE ;p.y++)
  {
    for (p.x = 0; p.x < UNI_SIZE; p.x++)
    {
     set_cell(board,p,get_cell(universe,p));
    }
  }
}
