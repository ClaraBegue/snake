#ifndef UNIVERSE_H
#define UNIVERSE_H

#define UNI_SIZE 30
#define BACKGROUND '-'
#define BLOCK '+'
#define LIMIT '#'
#define SEED '*'
#define SNAKE_BODY '@'
#define SNAKE_HEAD '0'

#include "queue.h"
#include "snake.h"

char get_cell (char[],point);
void set_cell (char[],point,char);
void set_cell_update(char[],point,char);
int update_universe (char[], snake*, queue*, queue*);
void print(char[]);
void copy(char[],char[]);

#endif
