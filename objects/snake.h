#ifndef SNAKE_H
#define SNAKE_H

#include "queue.h"

typedef enum
{
  NO_MOVE,
  LEFT,
  UP,
  RIGHT,
  DOWN,
} direction;

typedef struct snake snake;

snake* init_snake();
void set_body (snake*, double_chained*);
snake* update_snake (snake*);
void incr_len (snake*);
int get_len (snake*);
direction get_dir (snake* s);
queue* get_body (snake*);
void new_dir (snake*,direction);

#endif
