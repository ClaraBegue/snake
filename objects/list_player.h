#ifndef LIST_PLAYER_H
#define LIST_PLAYER_H

typedef struct player player;

typedef struct double_chained_player double_chained_player;

typedef struct list_player list_player;

player* new_player (char*);
void update_score (player*, int);
char* name_player (player*);
int score_player (player*);
list_player* new_list_player (void);
void add_player (list_player*,player*);
list_player* get_next (list_player*);
player* get_head_player (list_player*);
player* get_tail_player (list_player*);
int empty (list_player*);

#endif
