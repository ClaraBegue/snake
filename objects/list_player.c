#include <stdlib.h>
#include <ncurses.h>
#include "list_player.h"

struct player
{
  char* name;
  int score;
};

struct double_chained_player
{
  player* player;
  struct double_chained_player* left;
  struct double_chained_player* right;
};

struct list_player
{
  double_chained_player* head;
  double_chained_player* tail;
};

player* new_player (char* name)
{
  player* player = malloc(sizeof(struct player));

  player->name = name;
  player->score = 0;
  return player;
}

void update_score (player* player, int score)
{
  player->score = score;
}

char* name_player (player* player)
{
  return player->name;
}
int score_player (player* player)
{
  return player->score;
}

list_player* new_list_player ()
{
  list_player* list;
  list = malloc(sizeof(struct list_player));
  list->head = NULL;
  list->tail = NULL;
  return list;
}

void add_player (list_player* listi, player* new_player)
{
  double_chained_player* new_list = malloc(sizeof(double_chained_player));

  new_list->player = new_player;
  new_list->right = listi->head;
  new_list->left = NULL;

  if (empty (listi))
  {
    listi->head = new_list;
    listi->tail = new_list;
  }
  else
  {
    listi->head->left = new_list;
    listi->head = new_list;
  }
}

list_player* get_next (list_player* list)
{
  list_player* new;
  new = malloc(sizeof(struct list_player));
  new->head = list->head;
  new->tail = list->tail->left;
  return new;
}

player* get_head_player (list_player* list)
{
  if (empty (list))
  {
    player* p;
    p = NULL;
    return p;
  }
  return list->head->player;
}

player* get_tail_player (list_player* list)
{
  if (empty (list))
  {
    player* p;
    p = NULL;
    return p;
  }
  return list->tail->player;
}

int empty (list_player* list)
{
  return (list->tail == NULL);
}
