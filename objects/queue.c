#include <stdlib.h>
#include "queue.h"
#include <ncurses.h>

struct double_chained
{
 point point;
 struct double_chained* left;
 struct double_chained* right;
};

struct queue
{
  double_chained* head;
  double_chained* tail;
};


queue* new_queue ()
{
   queue* q = malloc(sizeof(struct queue));
   q->head = NULL;
   q->tail = NULL;
   return q;
}

point new_point()
{
  point p;
  p.x = -1;
  p.y = -1;
  return p;
}

int empty_queue (queue* q)
{
     return (q->tail == NULL);
}

void add_point (queue* q, point p)
{
  double_chained* c = malloc(sizeof(double_chained));
  c->point = p;
  c->right = q->head;
  c->left = NULL;
  if (empty_queue (q))
  {
	  q->head = c;
	  q->tail = c;
  }
  else
  {
	  q->head->left = c;
	  q->head = c;
  }
}

double_chained* get_head_chained(queue* q)
{
  if (empty_queue(q))
  {
    return (NULL);
  }
  return q->head;
}

point get_point_chained (double_chained* c)
{
  if (c == NULL)
  {
    return new_point();
  }
  return c->point;
}

double_chained* get_left_chained (double_chained* c)
{
  if (c == NULL)
  {
    return (NULL);
  }
  return c->left;
}

double_chained* get_right_chained (double_chained* c)
{
  if (c == NULL)
  {
    return (NULL);
  }
  return c->right;
}

void set_head (queue* q, double_chained* c)
{
  if (empty_queue(q))
  {
    q = malloc(sizeof(struct queue));
    q->head = c;
  }
  q->head = c;
}

point get_head(queue* q)
{
  if(empty_queue (q))
  {
	  return new_point();
  }
  return q->head->point;
}

point get_tail(queue* q)
{
  if(empty_queue (q))
  {
	  return new_point();
  }
  return q->tail->point;
}

void pop_queue (queue* q)
{
     q->tail = q->tail->left;
}
