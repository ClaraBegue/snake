#include <stdlib.h>
#include <ncurses.h>
#include "universe.h"
#include "queue.h"
#include "snake.h"

struct snake
{
  queue* body;
  int len;
  direction dir;
};


snake* init_snake ()
{
  snake* s = malloc(sizeof(struct snake));
  point p;
  s->body = new_queue();
  s->len = 3;
  s->dir = RIGHT;

  p.y = UNI_SIZE/2;

  for (p.x = 1 ; p.x < 4; p.x++)
    add_point(s->body,p);
  return s;
}

void set_body(snake* s, double_chained* q)
{
  set_head(s->body, q);
}

snake* update_snake(snake* s)
{
  point p;
  snake* new_s = malloc(sizeof(struct snake));
  new_s->len = s->len;
  new_s->dir = s->dir;
  // pop_queue (&s->body);
  p = get_head (s->body);

  switch (s->dir)
  {
    case (LEFT):
      p.x -= 1;
      break;
    case (RIGHT):
      p.x += 1;
      break;
    case UP:
      p.y -= 1;
      break;
    case DOWN:
      p.y += 1;
      break;
    default:
      break;
  }
  add_point(get_body(s),p);
  p = get_head (s->body);

  return s;
}

void incr_len(snake* s)
{
  s->len += 1;
}

int get_len (snake* s)
{
  return (s->len);
}

direction get_dir (snake* s)
{
  return (s->dir);
}

queue* get_body(snake* s)
{
  return (s->body);
}

void new_dir (snake* s,direction direc)
{
  s->dir = direc;
}
