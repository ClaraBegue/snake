.SUFFIXES:
.PHONY: default all clean mrproper

C:= gcc
LDFLAGS:= -lncurses
TARGET:= game

C_FILES:= $(wildcard *.c */*.c)
H_FILES:= $(wildcard *.h */*.h)
O_FILES:= $(C_FILES:%.c=%.o)

I_MARKS:= $(dir $(H_FILES))
CFLAGS:= -Wall -Wextra -g $(I_MARKS:%=-I %)

default: all

all: $(TARGET)

$(O_FILES): %.o: %.c $(H_FILES)
	$(C) $(CFLAGS) -c $< -o $@

$(TARGET): $(O_FILES)
	$(C) $(CFLAGS) $^ -o $@ ${LDFLAGS}

play: all
	./game

clean:
	-rm $(O_FILES)
	-rm $(TARGET)
	-rm -r *~
