#include <ncurses.h>
#include "list_player.h"

void end_game()
{
  char c;
  printw("\n \n \n  Pffff...");
  flushinp();
  scanw("%c",&c);
}

void new_best_score (int score)
{
  clear();
  printw("\n \n");
  printw("      __    _  _____ __            __         ___  _____  _____  _______      \n");
  printw("     |  \\  | ||  ___|\\ \\          / /        | _ \\|  ___||  ___||__  ___|     \n");
  printw("     |   \\ | || |__   \\ \\        / /         | __/| |__  | |___    | |        \n");
  printw("     | |\\ \\| ||  __|   \\ \\  /\\  / /          | \\_ |  __| |___  |   | |        \n");
  printw("     | | \\   || |___    \\ \\/  \\/ /           | _ \\| |___  ___| |   | |        \n");
  printw("     |_|  \\__||_____|    \\__/\\__/            |___/|_____||_____|   |_|        \n\n\n");
  printw("                _____  _____  _____  _____  _____     __ \n");
  printw("               |  ___||  ___|| ___ ||  _  ||  ___|   |  | \n");
  printw("               | |___ | |    ||   ||| |_| || |__     |  | \n");
  printw("               |___  || |    ||   |||   __||  __|    |__| \n");
  printw("                ___| || |___ ||___||| |\\ \\ | |___     __  \n");
  printw("               |_____||_____||_____||_| \\_\\|_____|   |__| \n");


  printw("\n\n\n   Your new best score is %d !",score);

  char c;
  flushinp();
  scanw("%c",&c);
}

void update_score_max(int diff, int score)
{
  FILE* score_max;
  int a;
  int b;
  int c;

  score_max = fopen ("before_and_after_turn/score.txt","r");
  fscanf(score_max,"%d %d %d",&a,&b,&c);
  freopen("before_and_after_turn/score.txt","w+",score_max);

  switch (diff)
    {
    case 1:
      if (a < score)
    	{
    	  a = score;
    	  new_best_score(score);
    	}
      break;

    case 2:
      if (b < score)
    	{
    	  b = score;
    	  new_best_score(score);
	    }
      break;

    case 3:
      if (c < score)
    	{
    	  c = score;
    	  new_best_score(score);
    	}
      break;
    }

  fprintf(score_max,"%d %d %d",a,b,c);
  fclose(score_max);
}

list_player* get_winner (list_player* list)
{
  list_player* winners;
  winners = new_list_player();

  if (1 - empty(list))
  {
    int max;
    list_player* new_list;

    max = score_player(get_tail_player(list));
    new_list = get_next(list);

    while (1 - empty(new_list))
    {
      if (score_player(get_tail_player(new_list)) > max)
      {
        max = score_player(get_tail_player(new_list));
      }
      new_list = get_next(new_list);
    }

    new_list = list;
    while (1 - empty(new_list))
    {
      if (score_player(get_tail_player(new_list)) == max)
      {
        add_player(winners, get_tail_player(new_list));
      }
      new_list = get_next(new_list);
    }
  }
  return winners;
}

void print_winners (list_player* list_p)
{
  list_player* winners;
  winners = get_winner(list_p);

  if (empty(get_next(winners)))
  {
    player* winner;
    winner = get_tail_player(winners);
    printw("  %s wins with %d point! \n", name_player(winner), score_player(winner) - 3);
    return;
  }

  printw("  ");
  while (1 - empty(get_next(get_next(winners))))
  {
    printw(" %s,",name_player(get_tail_player(winners)));
    winners = get_next(winners);
  }
  player* last_winner;
  last_winner = get_tail_player(get_next(winners));
  printw("%s and %s win ",name_player(get_tail_player(winners)),name_player(last_winner));
  printw("with %d points!\n",score_player(get_tail_player(winners)) - 3);
}
