#include <stdlib.h>
#include <ncurses.h>
#include "universe.h"
#include "snake.h"

/*Choose difficulity*/

char chose_diff ()
{
  char diff ;

  printw ("Welcome to the best game of the year !! \n \n");
  printw ("Chose your difficulity: \n   1: baby level \n   2: can cause damage \n   3: fly, you fools \n");
  printw ("\n \nTo escape, press q...");

  do
  {
    diff = getch();
  }while (diff != '1' && diff != '2' && diff !='3' && diff != 'q');
  flushinp();

  clear();
  return diff;
}

/*Create universe and add blocks into*/


void create_bloc (char universe[],point center)
{
  point p;

  for(p.y = center.y - 1; p.y <= center.y + 1; p.y++)
    {
      for(p.x = center.x - 1; p.x <= center.x + 1; p.x++)
	if (get_cell(universe,p) != SNAKE_BODY && get_cell(universe,p) != SNAKE_HEAD)
	  set_cell(universe,p,BLOCK);
    }
}

int is_ok (char universe[], point p)
{
  if (p.y >= UNI_SIZE/2 - 1 && p.y <= UNI_SIZE/2 + 1 &&
      p.x <= 7)
    return 0;

  return (get_cell(universe,p) == BACKGROUND);
}

void add_diff(char universe[], int diff)
{
  int nb_bloc;
  int i;
  point p;

  switch (diff)
    {
    case (1):
      nb_bloc = 3;
      break;
    case (2):
      nb_bloc = 5;
      break;
    case (3):
      nb_bloc = 10;
      break;
    }

  for(i = 0; i < nb_bloc; i++)
    {
      do
	{
	  p.x = rand()%(UNI_SIZE-6)+3;
	  p.y = rand()%(UNI_SIZE-6)+3;
	}
      while (1 - is_ok(universe,p));
      create_bloc(universe,p);
    }
}

void init_universe (char universe [], snake* s, int diff)
{
  point p;

  for (p.y = 1; p.y <UNI_SIZE -1;p.y++)
  {
    for (p.x = 1; p.x < UNI_SIZE-1; p.x++)
    set_cell(universe,p,BACKGROUND);
  }

  p.x = 0;
  for (p.y = 0; p.y < UNI_SIZE; p.y++)
  {
    set_cell(universe,p,LIMIT);
    p.x = UNI_SIZE - 1;
    set_cell(universe,p,LIMIT);
    p.x = 0;
  }

  p.y = 0;
  for (p.x = 0; p.x < UNI_SIZE; p.x++)
  {
    set_cell(universe, p, LIMIT);
    p.y = UNI_SIZE-1;
    set_cell(universe,p,LIMIT);
    p.y=0;
  }
  double_chained* c;
  double_chained* reserved;
  c = get_head_chained(get_body(s));
  reserved = c;
  set_cell(universe,get_point_chained(c),SNAKE_HEAD);
  c = get_right_chained(c);

  while (get_right_chained(c) != NULL)
  {
    set_cell(universe,get_point_chained(c),SNAKE_BODY);
    c = get_right_chained(c);
  }
  set_cell(universe,get_point_chained(c),SNAKE_BODY);

  add_diff(universe,diff);

  set_body(s, reserved);
}
