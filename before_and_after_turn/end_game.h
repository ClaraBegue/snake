#ifndef END_GAME_H
#define END_GAME_H

#include "list_player.h"

void end_game (void);
void update_score_max (int, int);
void print_winners (list_player*);

#endif
