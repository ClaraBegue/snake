#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ncurses.h>
#include <time.h>
#include <sys/time.h>
#include "universe.h"
#include "snake.h"
#include "key_pressed.h"
#include "seed.h"
#include "end_game.h"

void update_speed(char** speed, int score)
{
  if (score > 20)
    *speed = "stty -icanon min 0 time 1";
  else if (score > 10)
    *speed = "stty -icanon min 0 time 2";
  else if (score > 4)
    *speed = "stty -icanon min 0 time 3";
}

int turn(snake* s, char universe[],int diff, queue* seeds_now, queue* seeds_previous)
{
  // param to move the snake
  char c;
  direction new;
  char* speed;

  // time param in case of bad key pressed
  struct timeval stop, start;
  struct timespec wait;
  long elapsed;
  long laps_time_nano;

  print(universe);
  srand(time(NULL));
  put_seed(universe, seeds_now, seeds_previous);
  move(UNI_SIZE,0);
  printw ("Score: %d", get_len(s) - 3);
  speed = "stty -icanon min 0 time 4";

  do
  {
    refresh();
    new = get_dir(s);
    system(speed); // define the duration of one iteration
    // last speed char is time between each frame (0.1s), we convert it in us
    laps_time_nano = (long)(speed[strlen(speed)-1] - '0') * 100000;

    gettimeofday(&start, NULL);
    if (read(STDIN_FILENO, &c, 1) == 1)
    {
      gettimeofday(&stop, NULL);
      flushinp();
      if (not_dir(c)) // if the key is not a direction, we wait the next ite
      {
        elapsed = stop.tv_usec - start.tv_usec; // time elapsed in microsecond
        wait.tv_sec = (laps_time_nano - elapsed) / 1000000; // waiting time in s
        wait.tv_nsec = (laps_time_nano - elapsed) * 1000; // waiting time in ns
        nanosleep(&wait, NULL);
      }
      else
        new = key_pressed(c,new);
    }

    new_dir(s,new);
    update_speed(&speed,get_len(s));
    flushinp();

  }while(update_universe(universe,s,seeds_now,seeds_previous));

  move(UNI_SIZE + 2 , 0);
  printw("LOOOOSER !!\n");

  update_score_max(diff,get_len(s) - 3);
  flushinp();
  scanw("%c",&c);
  flushinp();
  clear();
  return get_len(s);
}
