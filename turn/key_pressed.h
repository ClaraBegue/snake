#ifndef KEY_PRESSED_H
#define KEY_PRESSED_H

#include "snake.h"

#define UP_KEY 'z'
#define DOWN_KEY 's'
#define LEFT_KEY 'q'
#define RIGHT_KEY 'd'

direction is_forbidden(direction);
direction key_pressed(int,direction);
int not_dir(int);

#endif
