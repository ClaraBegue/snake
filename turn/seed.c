#include <stdlib.h>
#include "seed.h"
#include "queue.h"
#include "universe.h"

void put_seed(char universe[], queue* seeds_now, queue* seeds_previous)
{
  point p;

  if (empty_queue(seeds_previous))
  {
    do
      {
        p.x = rand()%(UNI_SIZE-1);
        p.y = rand()%(UNI_SIZE-1);
      }
    while (get_cell(universe,p) != '-');
  }
  else
  {
    p = get_tail(seeds_previous);
    pop_queue(seeds_previous);
  }

  add_point(seeds_now, p);
  set_cell_update(universe,p,'*');
}
