#include <stdlib.h>
#include <ncurses.h>
#include "snake.h"
#include "universe.h"
#include "turn.h"
#include "list_player.h"
#include "end_game.h"

int is_answer(char c)
{
  int answer;
  answer = (c-'y') * (c-'Y') * (c-'n') * (c-'N');
  return (answer == 0);
}

int is_yes(char answer)
{
  return (answer == 'Y' || answer == 'y');
}

int chose_tournament(void)
{
  char answer;

  echo();
  printw("Do you want to play in tournament mode ? [y or n] ");

  flushinp();
  scanw("%c\n", &answer);
  printw("%c\n", answer);

  while (1 - is_answer(answer))
  {
    printw(" Please answer y or n. ");
    flushinp();
    scanw("%c\n", &answer);
    printw("%c\n", answer);
  }

  noecho();
  clear();
  return (is_yes(answer));
}

list_player* get_name ()
{
  int nb_p;
  int i;
  list_player* list_p = new_list_player();

  echo();
  printw ("How many players ? ");
  flushinp();
  scanw("%d\n", &nb_p);

  while(nb_p < 2 || nb_p >= 5)
  {
    printw ("You must be more than 2 players and less than 5... \nHow many players ? ");
    flushinp();
    scanw("%d\n", &nb_p);
  }

  printw("\n Who are these %d players ?\n", nb_p);
  for (i = 0 ; i < nb_p ; i++)
  {
    char* name = malloc (sizeof (*name) * 256);
    scanw("%s\n", name);
    player* p = new_player(name);
    add_player(list_p,p);
  }
  noecho();
  return list_p;
}

void turn_tournament (snake* s,char universe[], int diff)
{
  list_player* list_p;
  char board [UNI_SIZE*UNI_SIZE];
  int score_player;
  char c;
  player* player_turn;
  queue* seeds_now;
  queue* seeds_previous;

  seeds_now = new_queue();
  seeds_previous = new_queue();

  list_p = get_name();
  list_player* list_p_loop;
  list_p_loop = list_p;

  while (1 - empty(list_p_loop))
  {
    copy(board,universe);

    player_turn = get_tail_player(list_p_loop);
    clear();
    printw("  It's %s's turn ! <3\n", name_player(player_turn));
    flushinp();
    scanw("%c\n",&c);

    s = init_snake(s);
    score_player = turn(s,board,diff,seeds_now,seeds_previous);

    update_score (player_turn,score_player);

    list_p_loop = get_next(list_p_loop);

    seeds_previous = seeds_now;
    seeds_now = new_queue();
  }

  print_winners(list_p);
  flushinp();
  scanw("%c\n",&c);
  clear();
}
