#include <ncurses.h>
#include "snake.h"
#include "key_pressed.h"

direction is_forbidden (direction dir)
{
  switch (dir)
    {
    case (UP):
      return DOWN;
      break;
    case (DOWN):
      return UP;
      break;
    case (LEFT):
      return RIGHT;
      break;
    case (RIGHT):
      return LEFT;
      break;
    default:
      return NO_MOVE;
      break;
    }
  return NO_MOVE;
}

direction key_pressed(int new,direction dir)
{
  direction forbidden;
  forbidden = is_forbidden (dir);
  if (UP != forbidden && new == UP_KEY)
    return UP;
  else if (LEFT != forbidden && new == LEFT_KEY)
    return LEFT;
  else if (DOWN != forbidden && new == DOWN_KEY)
    return DOWN;
  else if (RIGHT != forbidden && new == RIGHT_KEY)
    return RIGHT;
  else
    return dir;
}

int not_dir(int new)
{
  switch (new)
  {
    case (UP_KEY):
      return 0;
      break;
    case (DOWN_KEY):
      return 0;
      break;
    case (LEFT_KEY):
      return 0;
      break;
    case (RIGHT_KEY):
      return 0;
      break;
    default:
      return 1;
      break;
  }
}
