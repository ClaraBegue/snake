#include <ncurses.h>
#include "begin_game.h"
#include "universe.h"
#include "snake.h"
#include "turn.h"
#include "tournament.h"
#include "end_game.h"

int main ()
{
  char universe [UNI_SIZE*UNI_SIZE] = {BACKGROUND};
  snake* s;
  char diff;
  char tournament;

  initscr();
  noecho();

  diff = chose_diff();

  while (diff != 'q')
    {
      s = init_snake();

      init_universe(universe,s,diff-'0');

      tournament = chose_tournament();

      if (tournament)
      {
        turn_tournament(s,universe,diff-'0');
      }
      else
      {
        queue* seeds_now;
        queue* seeds_previous;

        seeds_now = new_queue();
        seeds_previous = new_queue();

        turn(s,universe,diff-'0',seeds_now,seeds_previous);
      }

      diff = chose_diff();
    }

  end_game();
  endwin();

  return 0;
}
